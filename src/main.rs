use std::fs;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;
use std::time::{Duration, Instant};

mod thread_pool;
use thread_pool::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:12345").expect("Failed to bind to 127.0.0.1:12345");
    let mut pool = ThreadPool::new(4);
    let start = Instant::now();
    for stream in listener
        .incoming()
        .take_while(|_| Instant::now() - start < Duration::new(10, 0))
    {
        let stream = stream.unwrap();
        println!("Connected: {:?}", stream);
        pool.execute(|| handle_connection(stream));
    }
    pool.join();
    std::thread::sleep(Duration::new(5, 0));
}

fn handle_connection(mut stream: TcpStream) {
    println!("Waiting for payloads");
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();
    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";
    let broken = b"GET /broken HTTP/1.1\r\n";
    let (status, filename) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK", "index.html")
    } else if buffer.starts_with(sleep) {
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK", "index.html")
    } else if buffer.starts_with(broken) {
        ("", "index.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND", "404.html")
    };
    let contents = fs::read_to_string(filename).unwrap();
    let response = format!(
        "{}\r\nContent-Length: {}\r\n\r\n{}",
        status,
        contents.len(),
        contents
    );
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
