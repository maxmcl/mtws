use std::collections::VecDeque;
use std::sync::{atomic, Arc, Condvar, Mutex};

struct Shared<F>
where
    F: FnOnce(),
{
    queue: Mutex<VecDeque<F>>,
    cv: Condvar,
    running: atomic::AtomicBool,
}

impl<F> Shared<F>
where
    F: FnOnce(),
{
    fn new() -> Self {
        println!("Creating Shared");
        Self {
            queue: Mutex::new(VecDeque::new()),
            cv: Condvar::new(),
            running: atomic::AtomicBool::new(true),
        }
    }
}

#[derive(Debug)]
struct Worker {
    thread: Option<std::thread::JoinHandle<()>>,
    id: usize,
}

impl Worker {
    fn new<F>(id: usize, shared: Arc<Shared<F>>) -> Self
    where
        F: FnOnce() + Send + 'static,
    {
        Self {
            id,
            thread: Some(std::thread::spawn(|| Self::work(shared))),
        }
    }

    fn work<F>(shared: Arc<Shared<F>>)
    where
        F: FnOnce() + Send + 'static,
    {
        // Consumes tasks from the queue until done
        println!("Starting worker");
        loop {
            println!("Waiting for task");
            let task: F;
            {
                let mut queue = shared
                    .cv
                    .wait_while(shared.queue.lock().unwrap(), |queue| {
                        queue.is_empty() && shared.running.load(atomic::Ordering::SeqCst)
                    })
                    .unwrap();
                match queue.pop_front() {
                    Some(t) => task = t,
                    _ => {
                        println!("Stopping worker");
                        break;
                    }
                }
            }
            println!("Running task!");
            task();
        }
    }

    pub fn join(&mut self) {
        println!("Joining {:?}", self);
        if let Some(thread) = self.thread.take() {
            thread.join().unwrap();
        }
    }
}

impl Drop for Worker {
    fn drop(&mut self) {
        self.join();
    }
}

pub struct ThreadPool<F>
where
    F: FnOnce(),
{
    pool: Vec<Worker>,
    shared: Arc<Shared<F>>,
}

impl<F> ThreadPool<F>
where
    F: FnOnce(),
{
    pub fn new(mut n_threads: usize) -> Self
    where
        F: FnOnce() + Send + 'static,
    {
        if n_threads < 1 {
            n_threads = 1;
        }
        println!("Creating pool");
        let mut pool = Vec::with_capacity(n_threads);
        let shared = Arc::new(Shared::new());
        for id in 0..n_threads {
            pool.push(Worker::new(id, shared.clone()));
        }
        Self { pool, shared }
    }

    pub fn join(&mut self) {
        println!("Joining thread pool");
        // Signal all workers to stop
        self.shared.running.store(false, atomic::Ordering::SeqCst);
        self.shared.cv.notify_all();
        for worker in &mut self.pool {
            worker.join();
        }
    }

    pub fn execute(&self, f: F) {
        // Push a job to the queue
        {
            println!("Pushing task");
            let mut queue = self.shared.queue.lock().unwrap();
            queue.push_back(f);
        }
        self.shared.cv.notify_one();
    }
}

impl<F> Drop for ThreadPool<F>
where
    F: FnOnce(),
{
    fn drop(&mut self) {
        self.join();
    }
}
